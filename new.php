<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://localhost:8080');

$name = strip_tags($_GET['login']); //имя пользователя
$channel = strip_tags($_GET['channel']); //название канала
$message = strip_tags($_GET['message']); //сообщение

$lastFile = __DIR__.'/channels/'.$channel.'/last.txt';
$dataFile = __DIR__.'/channels/'.$channel.'/data.json';


$descriptor = fopen($dataFile, 'r+');
flock($descriptor, LOCK_EX);

$data = json_decode(fread($descriptor, filesize($dataFile)));

$last = 1 + file_get_contents($lastFile);
$data[] = ['id' => $last, 'user' => $name, 'content' => $message];
//var_dump($data);
fseek($descriptor, 0);
fwrite($descriptor, json_encode($data, JSON_UNESCAPED_UNICODE));

//$last = file_get_contents($lastFile);

flock($descriptor, LOCK_UN);
fclose ($descriptor);

file_put_contents($lastFile, $last);

//clearstatcache()