<?php
header('Content-Type: application/json');
header('Access-Control-Allow-Origin: http://localhost:8080');

$startTime = microtime(true); //время начала обработки лонг пулл запроса
$name = strip_tags($_GET['login']); //имя пользователя
$channel = strip_tags($_GET['channel']); //название канала
$offset = (int) $_GET['offset']; //смещение (сколько сообщений пользователь уже прочитал начиная от начала)

$lastFile = __DIR__.'/channels/'.$channel.'/last.txt';
$dataFile = __DIR__.'/channels/'.$channel.'/data.json';

while(1){

    //Смещениме, запрошенное пользователем, меньше, чем количество сообщений?
    if(file_get_contents($lastFile) >= $offset){
        $data = json_decode(file_get_contents($dataFile));
        $data = array_slice($data, $offset);

        echo  json_encode($data);
        break;
    }

    //Мы работаем больше 20 секунд?
    if((microtime(true) - $startTime) > 20){
        echo '{"type": "no changes"}';
        break;
    }

    sleep(0.2); //отдохнем секундочку
}
